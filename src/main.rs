use std::net::{IpAddr, SocketAddr, UdpSocket};

fn main() {
    let address = SocketAddr::new(IpAddr::from([0, 0, 0, 0]), 14550);
    let socket = UdpSocket::bind(address).unwrap();

    let mut buffer = vec![0; 4096];
    while let Ok((_, addr)) = socket.recv_from(&mut buffer) {
        match mavlink_protocol::from_bytes(&buffer) {
            Ok(message) => {
                let json = serde_json::to_string(&message).unwrap();
                println!("Received from: {addr}, Content: {json}");
            }
            Err(e) => {
                println!("{e}");
            }
        };
    }
}
