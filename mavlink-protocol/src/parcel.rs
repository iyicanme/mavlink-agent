use std::io::{Cursor, Read};
use std::ops::Shl;

use bytes::Buf;

pub(crate) struct Parcel<'a>(Cursor<&'a [u8]>);

impl<'a> Parcel<'a> {
    pub(crate) fn new(buffer: &'a [u8]) -> Self {
        Self(Cursor::new(buffer))
    }

    pub(crate) fn get_u8(&mut self) -> u8 {
        self.0.get_u8()
    }

    pub(crate) fn get_i8(&mut self) -> i8 {
        self.0.get_i8()
    }

    pub(crate) fn get_u16(&mut self) -> u16 {
        #[cfg(target_endian = "little")]
        {
            self.0.get_u16_le()
        }
        #[cfg(target_endian = "big")]
        {
            self.0.get_u16()
        }
    }

    pub(crate) fn get_i16(&mut self) -> i16 {
        #[cfg(target_endian = "little")]
        {
            self.0.get_i16_le()
        }
        #[cfg(target_endian = "big")]
        {
            self.0.get_i16()
        }
    }

    pub(crate) fn get_u24(&mut self) -> u32 {
        #[cfg(target_endian = "little")]
        {
            let first: u32 = self.0.get_u8().into();
            let second: u32 = self.0.get_u8().into();
            let third: u32 = self.0.get_u8().into();

            first | second.shl(8) | third.shl(16)
        }
        #[cfg(target_endian = "big")]
        {
            let first = self.0.get_u8() as u32;
            let second = self.0.get_u8() as u32;
            let third = self.0.get_u8() as u32;

            first.shl(16) | second.shl(8) | third
        }
    }

    pub(crate) fn get_u32(&mut self) -> u32 {
        #[cfg(target_endian = "little")]
        {
            self.0.get_u32_le()
        }
        #[cfg(target_endian = "big")]
        {
            self.0.get_u32()
        }
    }

    pub(crate) fn get_i32(&mut self) -> i32 {
        #[cfg(target_endian = "little")]
        {
            self.0.get_i32_le()
        }
        #[cfg(target_endian = "big")]
        {
            self.0.get_i32()
        }
    }

    pub(crate) fn get_u64(&mut self) -> u64 {
        #[cfg(target_endian = "little")]
        {
            self.0.get_u64_le()
        }
        #[cfg(target_endian = "big")]
        {
            self.0.get_u64()
        }
    }

    pub(crate) fn get_i64(&mut self) -> i64 {
        #[cfg(target_endian = "little")]
        {
            self.0.get_i64_le()
        }
        #[cfg(target_endian = "big")]
        {
            self.0.get_i64()
        }
    }

    pub(crate) fn get_bytes(&mut self, length: usize) -> Vec<u8> {
        let mut bytes = vec![0; length];
        self.0.read_exact(&mut bytes).expect("not enough bytes");

        bytes
    }
}
