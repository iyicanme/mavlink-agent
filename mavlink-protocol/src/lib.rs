use parser::{Parse, Parser};
use result::Result;
use types::Message;

mod error;
mod parcel;
mod parser;
mod result;
mod types;

/// # Errors
/// Returns an Err if message has an invalid magic number or of a unrecognized type
pub fn from_bytes(bytes: &[u8]) -> Result<Message> {
    let mut parser = Parser::from_bytes(bytes);

    Parse::parse(&mut parser)
}
