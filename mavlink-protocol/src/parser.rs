use crate::parcel::Parcel;
use crate::result::Result;

pub struct Parser<'a> {
    input: Parcel<'a>,
    pub(crate) payload_length: u8,
    pub(crate) is_signed: bool,
    pub(crate) message_id: u32,
}

impl<'a> Parser<'a> {
    #[must_use]
    pub(crate) fn from_bytes(bytes: &'a [u8]) -> Self {
        Self {
            input: Parcel::new(bytes),
            payload_length: 0,
            is_signed: false,
            message_id: 0,
        }
    }
}

impl<'a> Parser<'a> {
    pub(crate) fn get_u8(&mut self) -> u8 {
        self.input.get_u8()
    }
    pub(crate) fn get_u16(&mut self) -> u16 {
        self.input.get_u16()
    }
    pub(crate) fn get_u24(&mut self) -> u32 {
        self.input.get_u24()
    }
    pub(crate) fn get_u32(&mut self) -> u32 {
        self.input.get_u32()
    }
    pub(crate) fn get_u64(&mut self) -> u64 {
        self.input.get_u64()
    }
    pub(crate) fn get_i8(&mut self) -> i8 {
        self.input.get_i8()
    }
    pub(crate) fn get_i16(&mut self) -> i16 {
        self.input.get_i16()
    }
    pub(crate) fn get_i32(&mut self) -> i32 {
        self.input.get_i32()
    }
    pub(crate) fn get_i64(&mut self) -> i64 {
        self.input.get_i64()
    }
    pub(crate) fn get_bytes(&mut self, length: usize) -> Vec<u8> {
        self.input.get_bytes(length)
    }
}

pub trait Parse
    where
        Self: Sized,
{
    fn parse(parser: &mut Parser) -> Result<Self>;
}
