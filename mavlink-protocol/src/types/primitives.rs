use std::ops::Deref;
use std::time::Duration;

use crate::parser::{Parse, Parser};
use crate::result::Result;

impl Parse for u8 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_u8())
    }
}

impl Parse for u16 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_u16())
    }
}

impl Parse for u32 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_u32())
    }
}

impl Parse for u64 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_u64())
    }
}

impl Parse for i8 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_i8())
    }
}

impl Parse for i16 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_i16())
    }
}

impl Parse for i32 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_i32())
    }
}

impl Parse for i64 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(parser.get_i64())
    }
}

pub struct U24(u32);

impl Parse for U24 {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(Self(parser.get_u24()))
    }
}

impl Into<u32> for U24 {
    fn into(self) -> u32 {
        self.0
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub(crate) struct Microseconds(Duration);

impl Parse for Microseconds {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(Self(Duration::from_micros(parser.get_u64())))
    }
}

impl Deref for Microseconds {
    type Target = Duration;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub(crate) struct Milliseconds(Duration);

impl Parse for Milliseconds {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(Self(Duration::from_millis(parser.get_u32().into())))
    }
}

impl Deref for Milliseconds {
    type Target = Duration;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
