use body::{
    AuthorizationKey, ChangeOperatorControlRequest, ChangeOperatorControlResponse, Ping,
    SystemStatus, SystemTime,
};
use primitives::U24;

use crate::error::Error;
use crate::parser::{Parse, Parser};
use crate::result::Result;

mod body;
mod primitives;

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct Message {
    header: Header,
    body: Body,
}

impl Parse for Message {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let message = Self {
            header: Parse::parse(parser)?,
            body: Parse::parse(parser)?,
        };

        Ok(message)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct Header {
    sequence_id: u8,
    system_id: u8,
    component_id: u8,
}

impl Parse for Header {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let magic_number = Parse::parse(parser)?;
        parser.payload_length = Parse::parse(parser)?;

        match magic_number {
            0xFE => {}
            0xFD => {
                let mandatory_flags = u8::parse(parser)?;
                parser.is_signed = (mandatory_flags & 0x01) == 0x01;

                let _ = u8::parse(parser)?;
            }
            _ => return Error::WrongMagicNumber(magic_number).into(),
        };

        let sequence_id = Parse::parse(parser)?;
        let system_id = Parse::parse(parser)?;
        let component_id = Parse::parse(parser)?;

        parser.message_id = U24::parse(parser)?.into();

        let header = Header {
            sequence_id,
            system_id,
            component_id,
        };

        Ok(header)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub enum Body {
    SystemStatus(SystemStatus),
    SystemTime(SystemTime),
    Ping(Ping),
    ChangeOperatorControlRequest(ChangeOperatorControlRequest),
    ChangeOperatorControlResponse(ChangeOperatorControlResponse),
    AuthorizationKey(AuthorizationKey),
}

impl Parse for Body {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let body = match parser.message_id {
            1 => Body::SystemStatus(Parse::parse(parser)?),
            2 => Body::SystemTime(Parse::parse(parser)?),
            4 => Body::Ping(Parse::parse(parser)?),
            5 => Body::ChangeOperatorControlRequest(Parse::parse(parser)?),
            6 => Body::ChangeOperatorControlResponse(Parse::parse(parser)?),
            7 => Body::AuthorizationKey(Parse::parse(parser)?),
            _ => return Error::UnhandledMessageType(parser.message_id).into(),
        };

        Ok(body)
    }
}
