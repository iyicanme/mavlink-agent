use std::ops::BitAnd;

use crate::error::Error;
use crate::parser::{Parse, Parser};
use crate::result::Result;

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub(crate) enum SensorStatus {
    NotAvailable,
    Disabled,
    Error,
    Healthy,
}

impl SensorStatus {
    pub(crate) const CONTAINS_EXTENDED_SENSORS: usize = 31;
}

impl Parse for [SensorStatus; 32] {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let present = parser.get_u32();
        let enabled = parser.get_u32();
        let healthy = parser.get_u32();

        (0..32)
            .map(|i| {
                let mask: u32 = 1 << i;

                let truth = (
                    present.bitand(mask) != 0,
                    enabled.bitand(mask) != 0,
                    healthy.bitand(mask) != 0,
                );

                match truth {
                    (false, _, _) => SensorStatus::NotAvailable,
                    (true, false, _) => SensorStatus::Disabled,
                    (true, true, false) => SensorStatus::Error,
                    (true, true, true) => SensorStatus::Healthy,
                }
            })
            .collect::<Vec<SensorStatus>>()
            .try_into()
            .map_err(|_| Error::Unexpected)
    }
}

struct SensorId;

impl SensorId {
    const GYROSCOPE: u32 = 0;
    const ACCELEROMETER: u32 = 1;
    const MAGNETOMETER: u32 = 2;
    const ABSOLUTE_PRESSURE: u32 = 3;
    const DIFFERENTIAL_PRESSURE: u32 = 4;
    const GPS: u32 = 5;
    const OPTICAL_FLOW: u32 = 6;
    const VISION_POSITION: u32 = 7;
    const LASER_POSITION: u32 = 8;
    const EXTERNAL_GROUND_TRUTH: u32 = 9;
    const ANGULAR_RATE_CONTROL: u32 = 10;
    const ATTITUDE_STABILIZATION: u32 = 11;
    const YAW_POSITION: u32 = 12;
    const Z_ALTITUDE_CONTROL: u32 = 13;
    const XY_POSITION_CONTROL: u32 = 14;
    const MOTOR_OUTPUTS: u32 = 15;
    const RC_RECEIVER: u32 = 16;
    const GYROSCOPE2: u32 = 17;
    const ACCELEROMETER2: u32 = 18;
    const MAGNETOMETER2: u32 = 19;
    const GEOFENCE: u32 = 20;
    const AHRS: u32 = 21;
    const TERRAIN: u32 = 22;
    const REVERSE_MOTOR: u32 = 23;
    const LOGGING: u32 = 24;
    const BATTERY: u32 = 25;
    const PROXIMITY: u32 = 26;
    const SATCOM: u32 = 27;
    const PREARM_CHECK: u32 = 28;
    const OBSTACLE_AVOIDANCE: u32 = 29;
    const PROPULSION: u32 = 30;
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub(crate) enum ControlRequestType {
    Obtain,
    Release,
}

impl Parse for ControlRequestType {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let request_type = match parser.get_u8() {
            0 => Self::Obtain,
            1 => Self::Release,
            _ => return Error::InvalidValue.into(),
        };

        Ok(request_type)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub(crate) enum ControlResponseType {
    Approved,
    WrongPasskey,
    UnsupportedEncryption,
    AlreadyUnderControl,
}

impl Parse for ControlResponseType {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let request_type = match parser.get_u8() {
            0 => Self::Approved,
            1 => Self::WrongPasskey,
            2 => Self::UnsupportedEncryption,
            3 => Self::AlreadyUnderControl,
            _ => return Error::InvalidValue.into(),
        };

        Ok(request_type)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub(crate) struct Passkey([u8; 25]);

impl Parse for Passkey {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(Self(
            parser
                .get_bytes(25)
                .try_into()
                .map_err(|_| Error::Unexpected)?,
        ))
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub(crate) struct Authkey([u8; 32]);

impl Parse for Authkey {
    fn parse(parser: &mut Parser) -> Result<Self> {
        Ok(Self(
            parser
                .get_bytes(32)
                .try_into()
                .map_err(|_| Error::Unexpected)?,
        ))
    }
}
