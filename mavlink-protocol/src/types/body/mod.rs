use field::{ControlRequestType, ControlResponseType, SensorStatus};

use crate::parser::{Parse, Parser};
use crate::result::Result;
use crate::types::body::field::{Authkey, Passkey};
use crate::types::primitives::{Microseconds, Milliseconds};

mod field;

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct SystemStatus {
    sensor_statuses: [SensorStatus; 32],
    load: u16,
    battery_voltage: u16,
    battery_current: i16,
    battery_remaining: i8,
    communication_drop_rate: u16,
    communication_error_count: u16,
    custom_error_count1: u16,
    custom_error_count2: u16,
    custom_error_count3: u16,
    custom_error_count4: u16,
    extended_sensor_statuses: Option<[SensorStatus; 32]>,
}

impl Parse for SystemStatus {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let sensor_statuses: [SensorStatus; 32] = Parse::parse(parser)?;

        let load = Parse::parse(parser)?;
        let battery_voltage = Parse::parse(parser)?;
        let battery_current = Parse::parse(parser)?;
        let battery_remaining = Parse::parse(parser)?;
        let communication_drop_rate = Parse::parse(parser)?;
        let communication_error_count = Parse::parse(parser)?;
        let custom_error_count1 = Parse::parse(parser)?;
        let custom_error_count2 = Parse::parse(parser)?;
        let custom_error_count3 = Parse::parse(parser)?;
        let custom_error_count4 = Parse::parse(parser)?;

        let has_extended_sensors =
            sensor_statuses[SensorStatus::CONTAINS_EXTENDED_SENSORS] == SensorStatus::Disabled;
        let extended_sensor_statuses = if has_extended_sensors {
            let s: [SensorStatus; 32] = Parse::parse(parser)?;
            Some(s)
        } else {
            None
        };

        let system_status = Self {
            sensor_statuses,
            load,
            battery_voltage,
            battery_current,
            battery_remaining,
            communication_drop_rate,
            communication_error_count,
            custom_error_count1,
            custom_error_count2,
            custom_error_count3,
            custom_error_count4,
            extended_sensor_statuses,
        };

        Ok(system_status)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct SystemTime {
    time_since_epoch_us: Microseconds,
    time_since_boot_ms: Milliseconds,
}

impl Parse for SystemTime {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let system_time = Self {
            time_since_epoch_us: Parse::parse(parser)?,
            time_since_boot_ms: Parse::parse(parser)?,
        };

        Ok(system_time)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct Ping {
    time_since_epoch_us: Microseconds,
    sequence: u32,
    target_system: u8,
    target_component: u8,
}

impl Parse for Ping {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let ping = Self {
            time_since_epoch_us: Parse::parse(parser)?,
            sequence: Parse::parse(parser)?,
            target_system: Parse::parse(parser)?,
            target_component: Parse::parse(parser)?,
        };

        Ok(ping)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct ChangeOperatorControlRequest {
    target_system: u8,
    request_type: ControlRequestType,
    version: u8,
    passkey: Passkey,
}

impl Parse for ChangeOperatorControlRequest {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let request = Self {
            target_system: Parse::parse(parser)?,
            request_type: Parse::parse(parser)?,
            version: Parse::parse(parser)?,
            passkey: Parse::parse(parser)?,
        };

        Ok(request)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct ChangeOperatorControlResponse {
    requesting_gcs: u8,
    request_type: ControlRequestType,
    response: ControlResponseType,
}

impl Parse for ChangeOperatorControlResponse {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let request = Self {
            requesting_gcs: Parse::parse(parser)?,
            request_type: Parse::parse(parser)?,
            response: Parse::parse(parser)?,
        };

        Ok(request)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, serde::Serialize, serde::Deserialize)]
pub struct AuthorizationKey {
    key: Authkey,
}

impl Parse for AuthorizationKey {
    fn parse(parser: &mut Parser) -> Result<Self> {
        let authorization_key = Self {
            key: Parse::parse(parser)?,
        };

        Ok(authorization_key)
    }
}
