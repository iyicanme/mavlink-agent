use std::fmt::{Display, Formatter};

use crate::result::Result;

#[derive(Debug)]
pub enum Error {
    WrongMagicNumber(u8),
    UnhandledMessageType(u32),
    Generic,
    Unexpected,
    InvalidValue,
}

impl<T> Into<Result<T>> for Error {
    fn into(self) -> Result<T> {
        Err(self)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Error::Generic => "generic error".to_owned(),
            Error::WrongMagicNumber(n) => format!("wrong magic number {n:#x}"),
            Error::UnhandledMessageType(n) => format!("unhandled message type {n}"),
            Error::Unexpected => "unexpected error".to_owned(),
            Error::InvalidValue => "invalid value".to_owned(),
        };

        write!(f, "{message}")
    }
}

impl std::error::Error for Error {}
